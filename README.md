# The command line budget tool

`au`, the atomic symbol of gold pronounced "Hey You !", is a very simple tool
that helps you to track and manage your monthly expanses and incomes.

> Currently WIP, see the [backlog](./backlog/README.md).

