use crate::core::error::ParameterError;
use chrono::{Datelike, Local, Month, NaiveDate};
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::Write;

pub mod loader;
pub mod error;

pub enum MonthFormat {
    NumericMonth,
    NumericYearMonth,
    AlphabeticMonth,
}

impl MonthFormat {
    pub fn from_str(arg: &str) -> Self {
        if arg.chars().all(char::is_alphabetic) {
            return MonthFormat::AlphabeticMonth;
        }
        if arg.chars().all(char::is_numeric) {
            return MonthFormat::NumericMonth;
        }
        MonthFormat::NumericYearMonth
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Tracker {
    pub year: i32,
    pub month: u32,
    pub budgets: Vec<Budget>,
}

impl Default for Tracker {
    fn default() -> Self {
        Self::load(
            Self::from_partial_numeric(&Local::now().date_naive().month().to_string()).unwrap()
        )
    }
}

impl Tracker {
    pub fn new(year: i32, month: u32) -> Self {
        Self {
            year,
            month,
            budgets : Vec::new()
        }
    }

    pub fn load(ym: (i32, u32)) -> Self {
        loader::load(&format!("tracker-{}{:02}", ym.0, ym.1))
            .unwrap_or_default()
            .try_deserialize::<Tracker>()
            .unwrap_or(Self::new(ym.0, ym.1))
    }

    pub fn from_str(arg: &str) -> Result<Self, ParameterError> {
        let ym = match MonthFormat::from_str(arg) {
            MonthFormat::NumericYearMonth => Self::from_full(arg),
            MonthFormat::NumericMonth => Self::from_partial_numeric(arg),
            MonthFormat::AlphabeticMonth => Self::from_partial_alpha(arg),
        }?;

        if ym.1 > 12 {
            Err(ParameterError::MonthFormatError)
        } else {
            Ok(Self::load(ym))
        }
    }

    fn from_full(arg: &str) -> Result<(i32, u32), ParameterError> {
        let year = arg[0..4]
            .parse::<i32>()
            .map_err(|_| ParameterError::MonthFormatError)?;
        let month = arg[5..]
            .parse::<u32>()
            .map_err(|_| ParameterError::MonthFormatError)?;
        Ok((year, month))
    }

    fn from_partial_numeric(arg: &str) -> Result<(i32, u32), ParameterError> {
        let year = Local::now().date_naive().year();
        let month = arg
            .parse::<u32>()
            .map_err(|_| ParameterError::MonthFormatError)?;
        Ok((year, month))
    }

    fn from_partial_alpha(arg: &str) -> Result<(i32, u32), ParameterError> {
        let year = Local::now().date_naive().year();
        let month = arg
            .parse::<Month>()
            .map_err(|_| ParameterError::MonthFormatError)?
            .number_from_month();
        Ok((year, month))
    }

    pub fn save(self) {
        let path = loader::path_for(&format!("tracker-{}{:02}", self.year, self.month));
        let mut file = File::options()
            .write(true)
            .create(true)
            .open(&path)
            .expect("Could not write tracker");
        writeln!(&mut file, "{}", toml::to_string(&self)
            .unwrap()).expect("Could not write tracker");
    }
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Budget {
    pub recurrent: bool,
    pub income: bool,
    pub tag: Option<String>,
    pub name: String,
    pub amount: f64,
    pub transfers: Vec<Transfer>,
}

impl Budget {
    pub fn new(
        recurrent: bool,
        income: bool,
        tag: Option<String>,
        name: String,
        amount: f64,
    ) -> Self {
        Self {
            recurrent,
            tag,
            name,
            amount,
            income,
            transfers: Vec::<Transfer>::new(),
        }
    }

    pub fn find_mut<'a>(budgets: &'a mut Vec<Self>, name: &str, income: bool) -> Option<&'a mut Self> {
        budgets.iter_mut().find(|b| b.name == name && b.income == income)
    }

}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Transfer {
    pub recurrent: bool,
    pub date: NaiveDate,
    pub amount: f64,
    pub slug: String,
}

impl Transfer {
    pub fn new(
        recurrent: bool,
        date: NaiveDate,
        amount: f64,
        slug: String,
    ) -> Self {
        Self {
            recurrent,
            date,
            amount,
            slug,
        }
    }

    pub fn register(self, budget: &mut Budget) {
        budget.transfers.push(self);
    }
}
