use crate::core::Tracker;
use chrono::{Local, NaiveDate};
use clap::{Parser, Subcommand};

pub mod processor;

#[derive(Parser)]
pub struct Args {
    /// Specify wich monthe accounting tracker to use.
    #[arg(
        short,
        long,
        value_parser = Tracker::from_str,
    )]
    pub month: Option<Tracker>,
    #[command(subcommand)]
    pub subcommand: Subcommands,
}

#[derive(Subcommand)]
pub enum Subcommands {
    /// Adds a new budget to the accounting tracker.
    New {
        /// Marks the budget recurrent.
        #[arg(short, long)]
        recurrent: bool,
        /// Adds budget to the income instead of expanse.
        #[arg(short, long)]
        income: bool,
        /// Specify a tag to the budget.
        #[arg(short, long)]
        tag: Option<String>,
        /// Name of the budget to add.
        name: String,
        /// Amount available for the budget.
        amount: f64,
    },
    List {
        /// List incomes instead of expanses.
        #[arg(short, long)]
        income: bool,
        /// Optional name of the budget to report.
        budget: Option<String>,
    },
    /// Adds a new expanse or income to the accounting tracker.
    Add {
        /// Marks the transfer recurrent.
        #[arg(short, long)]
        recurrent: bool,
        /// Adds the transfer to the income instead of expanse.
        #[arg(short, long)]
        income: bool,
        /// Specify the date of the transfer in the format YYYY-MM-DD
        #[arg(
            short,
            long,
            value_parser = |arg: &str| NaiveDate::parse_from_str(&arg, "%Y-%m-%d"),
            default_value_t = Local::now().date_naive()
        )]
        date: NaiveDate,
        /// Short description to indentify the transfer.
        #[arg(short, long)]
        slug: Option<String>,
        /// Amount of the transger.
        amount: f64,
        /// Optional name of the budget concerned by the transfer
        #[arg(default_value = "To sort")]
        budget: String,
    },
    /// Reports budgets consumption of the accounting tracker.
    Report {
        /// Outputs a full report with all budgets details.
        #[arg(short, long)]
        full: bool,
        /// Only used if an income budget is provided.
        #[arg(short, long)]
        income: bool,
        /// Optional name of the budget to report.
        budget: Option<String>,
    },
}
