use crate::core::{Tracker, Budget, Transfer};
use crate::command::Subcommands;

pub struct Processor {
    tracker: Tracker
}

impl Processor {
    pub fn new(tracker: Tracker) -> Self {
        Self {
            tracker
        }
    }

    pub fn process(mut self, command: Subcommands) {
        match command {
            Subcommands::New { recurrent, income, tag, name, amount, } => {
                self.tracker.budgets.push(Budget::new(recurrent, income, tag, name, amount));
                self.tracker.save();
            }
            Subcommands::List { income: _, budget: _, } => {
                println!("List");
            }
            Subcommands::Add { recurrent, income, date, amount, budget, slug, } => {
                let transfer = Transfer::new(recurrent, date, amount, match slug {
                    None => budget.clone(),
                    Some(slug) => slug
                });
                let target = match Budget::find_mut(&mut self.tracker.budgets, &budget, income) {
                    None => {
                        let target = Budget::new(false, income, None, String::from(budget), 0.00);
                        self.tracker.budgets.push(target);
                        self.tracker.budgets.last_mut().unwrap()
                    },
                    Some(b) => b
                };
                transfer.register(target);
                self.tracker.save();
            }
            Subcommands::Report { full, income, budget, } => {
                println!("Report");
                println!("full:{full}");
                println!("income:{income}");
                if let Some(budget) = budget {
                    println!("budget:{budget}");
                }
            }
        }
    }
}
