use crate::command::Args;
use crate::command::processor::Processor;
use clap::Parser;

mod command;
mod core;

fn main() {
    let args = Args::parse();
    Processor::new(args.month.unwrap_or_default()).process(args.subcommand);
}
