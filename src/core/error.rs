use std::error::Error;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug)]
pub enum ParameterError {
    MonthFormatError,
}

impl Error for ParameterError {}

impl Display for ParameterError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            ParameterError::MonthFormatError => {
                write!(f, "Invalid month format")
            }
        }
    }
}
