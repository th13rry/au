use config::{Config, ConfigError};
use std::path::PathBuf;
use xdg::BaseDirectories;

pub fn load(tracker: &str) -> Result<Config, ConfigError> {
    let xdg_dirs = BaseDirectories::with_prefix("au").unwrap();
    let mut path = xdg_dirs.get_config_home();
    path.push(tracker);
    Config::builder()
        .add_source(config::File::from(path))
        .build()
}

pub fn path_for(tracker: &str) -> PathBuf {
    let xdg_dirs = BaseDirectories::with_prefix("au").unwrap();
    let mut path = xdg_dirs.get_config_home();
    path.push(tracker);
    path.set_extension("toml");
    path
}
