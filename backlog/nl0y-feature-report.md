# Feature - Report

## Purpose
Being able to report how much of the budget is already consumed or provided.

## Usage
```zsh
Usage: au [--month <month>] report [--full] [--income] [<budget>]

Reports how much of the budget is already consumed or provided.

Arguments:
  <budget>        Optional name of the budget concern by the report.

Flags:
  -m, --month <month>     Specify which month accounting tracker to use.
  -i, --income            Only useful if a budget is provided.
                          Reports on an income budget.
  -f, --full              Outputs a full report with all budget details.
```

## Format
### Parameter `month`
See [Technical-Global month option](./f6t2-technical-global-month-option)

### Parameter `budget`
See section **Parameter `name`** in [Feature-New budget](./f3yy-feature-new-budget.md).

## Output
### Success
In case of success, the output must report the global state of current month.
Monthly state must report :
  - The total of all expanses of the month,
  - The ratio between actual expanses vs planned expanses,
  - The total of all incomes of the month,
  - The ratio between actual incomes vs planned incomes,
  - The impact on savings calculated as `<total income>-<total expanse>`.
```
[<month>]
   Expanse
  <total expanse amount><currency> total
  <ratio>% of planned

   Income
  <total income amount><currency> total
  <ratio>% of planned

   Savings
  <sign><calculated impact><currency> savings
```

In case of a budget provided, reports the budget state.
```
# Expanse budget examples
[<budget>]
   You already completely consumed the budget.
  <amount><currency> left
  <ratio>% consumed

[<budget>]
  <amount><currency> left
  <ratio>% consumed

# Income budget examples
[<budget>]
   You reached the income budget.
  <amount><currency> missing
  <ratio>% provided

[<budget>]
  <amount><currency> missing
  <ratio>% provided
```

### Errors
#### Unknown budget
#### Invalid month

## Behaviour
### Default
Reports all expanse budgets from the current month's accounting tracker.

### Options
#### At `au` level
##### Specify month
Allow to target the provided month instead of current one.
See [Technical-Global month option](./f6t2-technical-global-month-option.md)

#### At `report` level
##### Specify income
By default, reports expanses budgets. In case one would like to list income
ones, it must be specified.

```zsh
au report [--income] [<budget]
# Or with single hyphen parameter
# au report -i [<budget]
```

##### Specify full
By default, the report computes the total of all budgets but does not provide
any informations on each budget. In case one wants a report on all budget in
addition of global reports, it must be provided.

##### Specify budget
By default the report computes the total of all budgets. In case one would like
a report on a specific budget, it must be provided.

#au
#feature
#report
#budget
