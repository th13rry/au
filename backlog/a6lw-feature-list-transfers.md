# Feature - List Transfers

## Purpose
Being able to list all transfers from current month's accounting tracker.

## Usage
```zsh
Usage: au [--month <month>] list [--income] [<budget>]

List all expanses or incomes from the current or given month accounting tracker.

Arguments:
  <budget>     Optional name of the budget to list all transfers

Flags:
  -m, --month <month>     Specify which month accounting tracker to update.
  -i, --income            List income transfers instead of expanse ones.
```

## Format
### Parameter `month`
See [Technical-Global month option](./f6t2-technical-global-month-option.md)

### Parameter `budget`
See section **Parameter `name`** in [Feature-New budget](./f3yy-feature-new-budget.md).

## Output
### Success
In case of success, the output must list all corresponding transfers, grouped
by budget. In case of a budget provided, a single group appears in the output.
A transfer is represented by :
  - A date,
  - An amount in a currency,
  - The indication of recurrency
```
#  indicates recurrency

[<budget>] [<recurrency>]
<date>  <amount><currency>  [<recurrency>] <description>
<date>  <amount><currency>  [<recurrency>] <description>
<date>  <amount><currency>  [<recurrency>] <description>

[<budget>] [<recurrency>]
<date>  <amount><currency>  [<recurrency>] <description>
<date>  <amount><currency>  [<recurrency>] <description>
```

**It might be interesting in the future to provide a way to specify an output
format, such as json, yaml or other.**

### Errors
#### Unknown budget
#### Invalid amount
#### Invalid month

## Behaviour
### Default
List all transfers from the current month's accounting tracker.

### Options
#### At `au` level
##### Specify month
Allow to target the provided month instead of current one.
See [Technical-Global month option](./f6t2-technical-global-month-option.md)

#### At `list` level
##### Specify income
By default, lists transfers from the expanses. In case one would like to list
incomes, it must be specified.

```zsh
au list [--income] [<budget]
# Or with single hyphen parameter
# au list -i [<budget]
```

##### Specify budget
By default all transfers are listed, regardless of budget. In case one would
like to list transfers for a specific budget, it must be provided.

#au
#feature
#expanse
#income
#transfer
#list
