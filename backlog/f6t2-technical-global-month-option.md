# Technical - Global Month Option

## Purpose
Allow to target the provided month instead of current one

## Usage
```zsh
au [--month <month>] <command>
# Or with single hyphen parameter
# au -m <month> <command>
```

## Format
### Parameter `month`
The month parameter can be provided:
  - An absolute month in time

#### Absolute
##### Month only is provided
  - The numeric index of the month (01 .. 12)
  - The abreviated name of the month on 3 letters (jan .. dec)
  - The full name of the month (january .. december)
##### Year is also specified
  - The only accepted format is YYYY-MM (2024-06)

#au
#technical
#global
#option
