# Feature - New Budget

## Purpose
Being able to add a new budget to either expanse or income.

## Usage
```zsh
Usage: au [--month <month>] new [--recurrent] [--income] [--tag <tag>]
          <name> <amount>

Add a new budget to the current or given month accounting tracker.

Arguments:
  <name>     Name of the budget to add
  <amount>   Amount available in the budget

Flags:
  -m, --month <month>     Specify which month accounting tracker to update.
  -r, --recurrent         Marks the budget recurrent.
  -i, --income            Add budget to the income instead of expanse.
  -t, --tag <tag>         Specify a tag to the budget.
```

## Format
### Parameter `name`
The name of the budget is a string that doesn't allow any whitespace other
than a space ` `.

#### Languages
The name may be provided in any language (meaning ideograms and non latin
alphabets).

#### Limitations
There's no length nor case limitation.

### Parameter `tag`
A tag is a string that doesn't allow any whitespace other than a space ` `.

#### Languages
A tag may be provided in any language (meaning ideograms and non latin
alphabets).

#### Limitations
There's no length nor case limitation.

## Output
### Success
In case of success, the output must show the created budget along with the
amount set for it.
```
 <Expanse or Income> budget <budget> has been set to <amount>€.
```

### Errors
#### Budget already exists
#### Invalid budget name
#### Invalid tag
#### Invalid amount
#### Invalid month

## Behaviour
### Default
Creates a new section in the current month's accounting tracker.
If the budget already exists, reports its existance and does nothing.

**It might be interesting in the future update the amount in case of budget
already existing**

### Options
#### At `au` level
##### Specify month
Allow to target the provided month instead of current one.
See [Technical-Global month option](./f6t2-technical-global-month-option.md)

#### At `new` level
##### Specify recurrent
Enable recurrent budget which will be copied from month to month
including all expanses or incomes.

**It might be interesting in the future to provide a way to specify frequency**

```zsh
au new [--recurrent] <name>
# Or with single hyphen parameter
# au new -r <name>
```

##### Specify income
By default, a budget is added to the expanses. In case one would like to add
income budget, it must be specified.

```zsh
au new [--income] <name>
# Or with single hyphen parameter
# au new -i <name>
```

##### Specify a tag
By default, no tag will be added to the budget. In case one would like to
provide a tag to group budgets, it must be specified.

```zsh
au new --tag <tag> <name>
# Or with single hyphen parameter
# au new -t <tag> <name>
```

#au
#feature
#budget
#new
