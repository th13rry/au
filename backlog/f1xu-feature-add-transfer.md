# Feature - Add Transfer

## Purpose
Being able to add a new transfer to current month's accounting tracker.

## Usage
```zsh
Usage: au [--month <month>] add [--recurrent] [--income] [--date <date>]
          [--slug <slug>] <amount> [<budget>]

Add a new expanse or income to the current or given month accounting tracker.

Arguments:
  <amount>        Amount of the transfer
  <budget>        Optional name of the budget concern by the transfer

Flags:
  -m, --month <month>     Specify which month accounting tracker to update.
  -d, --date <date>       Specify the date of the transfer.
  -r, --recurrent         Marks the transfer recurrent.
  -i, --income            Add transfer to the income instead of expanse.
  -s, --slug              Short description to identify the transfer.
```

## Format
### Parameter `month`
See [Technical-Global month option](./f6t2-technical-global-month-option)

### Parameter `date`
The date of the transfer in the format `YYYY-MM-DD`. Examples:
  - `2024-06-24`
  - `2024-01-01`

### Parameter `amount`
The amount of the transfer in euros.

#### Currencies
At this time, only handles euros.

**It might be interesting in the future to handle different currencies.**

#### Notation
An amount support various notations :
  - Without a thousand separator. Examples:
    - `1234`
  - With ` ` as the thousand separator. Examples:
    - `1 234`
  - With `,` as the thousand separator. Examples:
    - `1,234`
  - With `.` as the decimal separator. Examples:
    - `1234.98`
    - `1 234.98`
    - `1,234.98`.
  - With `,` as the decimal separator if present exactly once. Examples:
    - `1234,98`
    - `1 234,98`

The currency might be provided before or after the amount. Examples:
  - `€1 234,98`
  - `€1,234.98`
  - `1234.98€`
  - `1 234.98€`

#### Limitations
The maximal allow value for a transfer is 1.7976931348623157E+308.

### Parameter `budget`
See section **Parameter `name`** in [Feature-New budget](./f3yy-feature-new-budget.md).

### Parameter `slug`
A short description to help identifying the transfer later on.
If not provided, the slug will match the `budget`.

## Output
### Success
#### Expanse
In case of success, the output must confirm the transfer addition and provide
how much money is left on the budget and the ratio of already consumed.
An alert will be raised in case of over 100% consumption.
```
 You already completely consumed the budget.
 Expanse has been added to <budget>.
  <amount><currency> left
  <ratio>% consumed
```

#### Income
In case of success, the output must confirm the income addition and provide
how much money is missing from the budget and the ratio of already provided.
```
 You reached the income budget.
 Income has been added to <budget>.
  <amount><currency> missing
  <ratio>% provided
```

### Errors
#### Invalid amount
#### Invalid month

## Behaviour
### Default
Adds a new transfer to the current month's accounting tracker.
If the budget is not specified, adds the transfer to the budget "To sort".

### Options
#### At `au` level
##### Specify month
Allow to target the provided month instead of current one.
See [Technical-Global month option](./f6t2-technical-global-month-option)

#### At `add` level
##### Specify date
By default, used the current date. In case one would like to specify a date for
the transfer, it must be specified.

##### Specify recurrent
Enable recurrent transfer which will be copied from month to month.

**It might be interesting in the future to provide a way to specify frequency**

```zsh
au add [--recurrent] <amount> [<budget>]
# Or with single hyphen parameter
# au add -r <amount> [<budget>]
```

##### Specify income
By default, a transfer is added to the expanses. In case one would like to add
incomes, it must be specified.

```zsh
au add [--income] <amount> [<budget]
# Or with single hyphen parameter
# au add -i <amount> [<budget]
```

##### Specify budget
By default a transfer is added to the "To sort" budget. In case one would like
to add a transfer to a specific budget, it must be specified.

#au
#feature
#expanse
#income
#transfer
#add
